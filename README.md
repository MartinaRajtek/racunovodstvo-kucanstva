# racunovodstvo-kucanstva


## Ime
Računovodstvo (vođenje) kućnog budžeta

## Opis
Preko izbornika će omogućavati pristup različitim podacima kao što su mjesećna,godišnja, srednja plaća, režije kućanstva i slične podatke korisne jodnome kućanstvu za vođenje vlastitog budžeta.

## Detaljniji opis
Omogućavat će pristup i upis informacija potrebne za lakše vođenje novca u kućanstvu. Na početku je potrebno unjeti plaću, režije, te kolko se potrošilo na potrebne namjernice tj. hranu. Tražit će se također i štednja i ostali troškovi za određeni mjesec gdje će biti u mogućnosti napisati 0. Korisnik će imati mogućnost pristupa informacijama koje će program izraćunat npr. aritmetička sredina mjesećne plaće, ukuopna štednja do određenog trenutka,  ukupni troškovi za jedan ili sve mjesece... 

## Cilj programa
Olakšani pregled na raspoloživ prihod. To jest lakše prećenje novca u kućanstvu kako bi se lakše i sigurnije raspolagalo s njime.

## Struktura projekta

## Status projekta
Projekt trenutno sadržava početak izbornika kako bi se omogučilo lakše raspoznavanje potrebnih funkcija (18.5.2023.). Početne opcije za izbornik su završene, nastasvljam sa potrebnim funkcijama za izračun aritmetičke sredine plaće i troškova, te izračun ukupnih troškova i štednje. Naknadno ću dodavati izbore u izbornik kako bi omogućila išćitavanje funkcija (25.5.2023.).
