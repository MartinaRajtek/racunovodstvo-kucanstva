#pragma once

#ifndef HEADER_H
#define HEADER_H

//3
typedef struct podaci {
	char mjesec[100];
	int placa;
	int rezije;
	int namjernice;
	int stednja;
	int minusStednja;
	int troskovi;
} PODACI;

void cratePodatke(PODACI podac);
void dodajPodatke();
void predledPodataka();
void traziPodatke();
void urediPodatke();
void obrisiPodatke();
int izlazIzPrograma();


#endif //HEADER_H