#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"

FILE* fp;

int main(void)
{
	int izbor;
	fp = fopen("budzet.txt", "ab+");
	fseek(fp, 0, SEEK_SET);

	//8
	printf("====================");
	printf("Odaberite zeljenu opciju:");
	printf("====================\n");

	printf("\t\t\t1. Unos podataka placa i troskova.\n");
	printf("\t\t\t2. Ispisivanje svih podataka.\n");
	printf("\t\t\t3. Ispis trazenog mjeseca.\n");
	printf("\t\t\t4. Uredivanje postojeceg mjeseca.\n");
	printf("\t\t\t5. Brisanje unesenog mjeseca.\n");
	printf("\t\t\t6. Kraj programa.\n");

	printf("=================================================================\n\n");


	printf("Unesi odabir: ");
	scanf("%d", &izbor);

	switch (izbor)
	{
	case 1:
		dodajPodatke();
		break;
	case 2:
		predledPodataka();
		break;
	case 3:
		traziPodatke();
		break;
	case 4:
		urediPodatke();
		break;
	case 5:
		obrisiPodatke();
		break;
	case 6:
		izlazIzPrograma();
		break;
	default:
		fclose(fp);
		exit(0);
	}

	fclose(fp);

	return 0;

}