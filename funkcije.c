#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "header.h"

FILE* fp;

void cratePodatke(PODACI podac)
{
	fp = fopen("budzet.txt", "a");	//16

	if (fp == NULL)
	{
		printf("Greska pri otvaranju datoteke.\n");
		return;
	}

	fprintf(fp, "====================\n");
	fprintf(fp, "\t\t Unesite potrebne podatke: \n");
	fprintf(fp, "====================\n\n");
	fprintf(fp, "Neto placa u EUR: %d\n", podac.placa);
	fprintf(fp, "Rezije za ovaj mjesec u EUR: %d\n", podac.rezije);
	fprintf(fp, "Troskovi namjerinica ovog mjeseca u EUR: %d\n", podac.namjernice);
	fprintf(fp, "Usteda ovog mjeseca u EUR: %d\n", podac.stednja);
	fprintf(fp, "Ostali troskovi ovog mjeseca u EUR: %d\n", podac.troskovi);

	fclose(fp);
}


void dodajPodatke()
{
	PODACI podac = { 0 };
	fp = fopen("budzet.txt", "ab");

	if (fp == NULL)
	{
		printf("Pogreska prilikom otvaranja datoteke.\n");
		return;
	}

	printf("\nUnesite mjesec za koji unosite podatke: ");
	scanf("%s", &podac.mjesec);  //mjesec je char, trazi se upis slovima

	printf("\nNeto placa u EUR: ");
	scanf("%d", &podac.placa);

	printf("\nRezije za ovaj mjesec u EUR: ");
	scanf("%d", &podac.rezije);

	printf("\nTroskovi namjerinca ovog mjeseca u EUR: ");
	scanf("%d", &podac.namjernice);

	printf("\nUsteda novaca ovog mjeseca u EUR ");
	scanf("%d", &podac.stednja);

	printf("\nOstali troskovi ovog mjeseca u EUR: ");
	scanf("%d", &podac.troskovi);

	printf("\n\n");

	fwrite(&podac, sizeof(PODACI), 1, fp);

	fclose(fp);
	main();
}


void predledPodataka()
{
	PODACI podac = { 0 };

	struct podaci* sviPodaci = NULL;
	int numPodaci = 0;

	while (fread(&podac, sizeof(PODACI), 1, fp) == 1)
	{
		numPodaci++;
		sviPodaci = (struct podaci*)realloc(sviPodaci, numPodaci * sizeof(struct podaci));	//14, 13

		strcpy(sviPodaci[numPodaci - 1].mjesec, podac.mjesec);

		sviPodaci[numPodaci - 1].placa = podac.placa;
		sviPodaci[numPodaci - 1].rezije = podac.rezije;
		sviPodaci[numPodaci - 1].namjernice = podac.namjernice;
		sviPodaci[numPodaci - 1].stednja = podac.stednja;
		sviPodaci[numPodaci - 1].troskovi = podac.troskovi;
	}
	//qsort(sviPodaci, numPodaci, sizeof(struct podaci));

	for (int i = 0; i < numPodaci; i++)
	{
		printf("\nPodaci za mjesec: %s\n", sviPodaci[i].mjesec);

		printf("\nNeto placa u EUR: %d\n", sviPodaci[i].placa);
		printf("\nRezije za ovaj mjesec u EUR %d\n", sviPodaci[i].rezije);
		printf("\nTroskovi namjernica ovog mjeseca u EUR: %d\n", sviPodaci[i].namjernice);
		printf("\nUsteda ovog mjeseca u EUR: %d\n", sviPodaci[i].stednja);
		printf("\nOstali troskovi ovog mjeseca u EUR: %d\n\n\n", sviPodaci[i].troskovi);
	}

	printf("\nPRITISNITE ENTER ZA POVRATAK");
	getch();
	system("cls");

	free(sviPodaci);	//15
	fclose(fp);
	main();
}


void traziPodatke()
{
	int a = 0;
	char id[50];
	char valuta[] = "EUR";	//12

	PODACI podac = { 0 };
	fp = fopen("budzet.txt", "ab+");
	fflush(stdin);

	printf("\n\t\t Trazite!\n\n");	//21

	puts("\nTrazeni mjesec: ");
	scanf("%s", id);

	while (fread(&podac, sizeof(PODACI), 1, fp) == 1)
	{
		if (strcmp(podac.mjesec, id) == 0)
		{
			printf("\nRezultati pretrage:\n");

			printf("\nMjesec: %s\n", podac.mjesec);
			printf("Neto placa u EUR: %d\n", podac.placa);
			printf("Rezije za ovaj mjesec u EUR: %d\n", podac.rezije);
			printf("Troskovi namjernica ovog mjeseva u EUR: %d\n", podac.namjernice);
			printf("Usteda ovog mjeseca u EUR: %d\n", podac.stednja);
			printf("Ostali troskovi ovog mjeseca u EUR: %d\n", podac.troskovi);
			a = 1;
		}
	}

	if (a != 1)
	{
		perror("Podaci nisu pronadeni.\n");	//19
	}

	printf("\nPRITISNITE ENTER ZA POVRATAK");
	getch();
	system("cls");

	fclose(fp);
	main();
}


void urediPodatke()
{
	fflush(stdin);

	PODACI podac = { 0 };
	FILE* fpt;
	printf("\n\t\t Uredite podatke!\n\n");

	char id[50];
	int a = 0;


	fflush(stdin);
	printf("\nKoje mjesec zelite prepravit: ");
	scanf("%s", id);


	fpt = fopen("ispis.txt", "wb");
	rewind(fp);

	int flag = 0, flag1 = 0, flag2;

	while (fread(&podac, sizeof(PODACI), 1, fp) == 1)
	{
		if (strcmp(podac.mjesec, id) == 0)
		{
			flag1++;
			flag2 = 1;
			fflush(stdin);

			printf("\n%s\" pronaden\n", id);
			flag = 10;

			printf("\nMjesec: %s\n", podac.mjesec);
			printf("Neto placa u EUR: %d\n", podac.placa);
			printf("Rezije za ovaj mjesec u EUR: %d\n", podac.rezije);
			printf("Troskovi namjernica ovog mjeseva u EUR: %d\n", podac.namjernice);
			printf("Usteda ovog mjeseca u EUR: %d\n", podac.stednja);
			printf("Ostali troskovi ovog mjeseca u EUR: %d\n", podac.troskovi);

			printf("\nUnesite nove podatke za mjesec %s:\n", id);

			printf("Neto placa u EUR: ");
			scanf("%d", &podac.placa);
			printf("Rezije za ovaj mjesec u EUR: ");
			scanf("%d", &podac.rezije);
			printf("Troskovi namjernica ovog mjeseca u EUR: ");
			scanf("%d", &podac.namjernice);
			printf("Usteda ovog mjeseca u EUR: ");
			scanf("%d", &podac.stednja);
			printf("Ostali troskovi ovog mjeseca u EUR: ");
			scanf("%d", &podac.troskovi);


			if (flag1 == 1 && flag2 == 1)
			{
				fseek(fpt, 0, 0);
			}
			else
			{
				fseek(fpt, (flag1 - 1) * sizeof(PODACI), 0);
			}
			fwrite(&podac, sizeof(PODACI), 1, fpt);

		}
		else
		{
			flag1++;
			fwrite(&podac, sizeof(PODACI), 1, fpt);
		}
	}

	if (flag != 10)
	{
		perror("Podaci nisu pronadeni.\n");
	}

	fclose(fp);
	fclose(fpt);

	//18
	remove("budzet.txt");
	rename("ispis.txt", "budzet.txt");

	fp = fopen("budzet.txt", "ab+");

	printf("\nPRITISNITE ENTER ZA POVRATAK");
	getch();
	system("cls");

	fclose(fp);
	main();
}


void obrisiPodatke()
{
	fflush(stdin);

	PODACI podac = { 0 };
	FILE* fpt;
	printf("\n\t\t Brisanje podataka!\n\n");

	char id[50];
	int a = 0;

	fflush(stdin);

	printf("\nKoji mjesec zelite obrisati: ");
	scanf("%s", id);

	fpt = fopen("brisanje.txt", "wb");
	rewind(fp);

	int flag = 0;

	while (fread(&podac, sizeof(PODACI), 1, fp) == 1)
	{
		if (strcmp(podac.mjesec, id) != 0)
		{
			fwrite(&podac, sizeof(PODACI), 1, fpt);
		}
		else if (strcmp(podac.mjesec, id) != 0)
		{
			puts("\a\nPodaci su obrisani.\n");
			flag = 10;
		}
	}

	if (flag != 10)
	{
		perror("Podaci za trazeni mjesec ne posteje.\n");
	}

	fclose(fp);
	fclose(fpt);

	remove("budzet.txt");
	rename("brisanje.txt", "budzet.txt");

	fp = fopen("budzet.txt", "ab+");

	printf("\nPRITISNITE ENTER ZA POVRATAK");
	getch();
	system("cls");

	fclose(fp);
	main();
}


int izlazIzPrograma()
{
	char ime[3] = { '\0' };
	printf("\n\nZelite li izaci?\n");
	scanf("%2s", ime);

	if (!strcmp("da", ime))
	{
		return 0;
	}
	return 1;
}